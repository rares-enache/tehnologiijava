/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.beans.Statement;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import javax.activation.DataSource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Rares
 */
@WebServlet(urlPatterns = {"/NewServlet"})
public class NewServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {

            InitialContext ctx = new InitialContext();

            /*
             * Lookup the DataSource, which will be backed by a pool
             * that the application server provides. DataSource instances
             * are also a good candidate for caching as an instance
             * variable, as JNDI lookups can be expensive as well.
             */
            DataSource ds
                    = (DataSource) ctx.lookup("java:comp/env/jdbc/MySQLDB");

            /*
             * The following code is what would actually be in your
             * Servlet, JSP or EJB 'service' method...where you need
             * to work with a JDBC connection.
             */
            Connection conn = null;
            Statement stmt = null;

            try {
                conn = ds.getConnection();

                /*
                 * Now, use normal JDBC programming to work with
                 * MySQL, making sure to close each resource when you're
                 * finished with it, which permits the connection pool
                 * resources to be recovered as quickly as possible
                 */
                stmt = (Statement) conn.createStatement();
                stmt.execute("SOME SQL QUERY");

                stmt.close();
                stmt = null;

                conn.close();
                conn = null;
            } finally {
                /*
                 * close any jdbc instances here that weren't
                 * explicitly closed during normal code path, so
                 * that we don't 'leak' resources...
                 */

                if (stmt != null) {
                    try {
                        stmt.close();
                    } catch (sqlexception sqlex) {
                        // ignore, as we can't do anything about it here
                    }

                    stmt = null;
                }

                if (conn != null) {
                    try {
                        conn.close();
                    } catch (sqlexception sqlex) {
                        // ignore, as we can't do anything about it here
                    }

                    conn = null;
                }
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
